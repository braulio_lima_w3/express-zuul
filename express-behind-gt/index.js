const port = 5000
const app = require('express')()
const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.use((req, res, next)=>{
    console.log(`[${new Date()}] [${req.method}] - ${req.url}`);
    next()
})

app.use('/hello', require('./hello-world'))

app.get(['/'], (req, res, next)=>{
    res.json({
        message : 'this is the index of the app'
    })
})

app.listen(port, ()=>{
    console.log(`Listening on port ${port}`);
});