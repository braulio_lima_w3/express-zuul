# Sobre este projeto

É uma amostra pequena do (spring zuul) utilizando dois
subprojetos. Um em express (que fica atras do zuul), e outro
spring boot (que contempla o zuul).

## Baixar Dependencias

Para o express

```bash
cd express-behind-gt
npm i
```

Para o zuul

```bash
cd border
mvn clean package
```

## Executar os projetos

Express:

```bash
cd express-behind-gt
npm run start
```

Para o zuul

```bash
cd border
mvn spring-boot:run
```

## Chamadas

Algumas chamadas que podem ser feitas:

### Express

    curl http://localhost:5000/

    curl http://localhost:5000/hello

### Spring boot + zuul

    curl http://localhost:8080/demo

    curl http://localhost:8080/demo/hello

    curl http://localhost:8080/actuator
